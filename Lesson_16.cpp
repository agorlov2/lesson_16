﻿// Lesson_16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>


using namespace std;

int main()
{
	setlocale(LC_ALL, "ru");

	time_t rawtime = time(NULL);
	struct tm timeinfo;
	int SizeArray;

	localtime_s(&timeinfo, &rawtime);// обновляет данные структуры timeinfo
	int CurrentDay = timeinfo.tm_mday;

	cout << "Сегодня число : " << CurrentDay << endl;
	cout << "Введите размер массива N x N,  N: ";
	cin >> SizeArray;

	int NumberString = CurrentDay % SizeArray;
	cout << "Остаток от деления текущей даты " << CurrentDay << " на " << SizeArray << " = " << NumberString << endl << endl;

	//Создание динамческого массива
	int** MyArray = new int* [SizeArray];
	for (int i = 0; i < SizeArray; i++)
	{
		MyArray[i] = new int[SizeArray];
	}

	//Заполнение массива
	for (int i = 0; i < SizeArray; i++)
	{
		for (size_t j = 0; j < SizeArray; j++)
		{
			MyArray[i][j] = i + j;
			cout << MyArray[i][j] << "\t";
		}
		cout << endl;
	}

	//Считает сумму элементов
	int SumElements = 0;
	for (int j = 0; j < SizeArray; j++)
	{
		//cout << MyArray[NumberString][j] << endl;
		SumElements += MyArray[NumberString][j];
	}
	cout << "Сумма элементов " << NumberString << " строки = " << SumElements << endl;

	//Очистка памяти
	for (int i = 0; i < SizeArray; i++)
	{
		delete[] MyArray[i];
	}
	delete[] MyArray;
	return 0;

}